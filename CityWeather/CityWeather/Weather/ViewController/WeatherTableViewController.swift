//
//  WeatherTableViewController.swift
//  CityWeather
//
//  Created by PM on 12/05/19.
//  Copyright © 2019 Hanumant Shegade. All rights reserved.
//

import UIKit
import CoreData

class WeatherTableViewController: UITableViewController {

    @IBOutlet var weatherViewModel: WeatherViewModel!
    
    let spinner = UIActivityIndicatorView()
    let loadingView = UIView()

    fileprivate lazy var fetchedResultsController: NSFetchedResultsController<Weather> = {
        let fetchRequest: NSFetchRequest<Weather> = Weather.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "cityName", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: APP_DELEGATE.MOC, sectionNameKeyPath: nil, cacheName: nil)
        return fetchedResultsController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        fetchData()
    }

    func setupUI() {
        self.title = "WEATHER"
        tableView.tableFooterView = UIView()
        // refreshControl?.addTarget(self, action: #selector(WeatherTableViewController.fetchData), for: .valueChanged)
    
        fetchedResultsController.delegate = self
        do {
            try fetchedResultsController.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Save Note")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
    }
    
    @objc func fetchData() {
        setLoadingScreen()
        weatherViewModel.invokeWeatherData {  [unowned self] in
            DispatchQueue.main.async {
                self.removeLoadingScreen()
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController.sections else {
            return 0
        }
        
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CityTableViewCell.self), for: indexPath) as? CityTableViewCell else {
            return UITableViewCell()
        }
        
        // Fetch Weather
        let weatherObj = fetchedResultsController.object(at: indexPath)
        cell.weather = weatherObj
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let weatherObj = fetchedResultsController.object(at: indexPath)
        guard let weatherDetailsVC = storyboard?.instantiateViewController(withIdentifier: String(describing: WeatherDetailsViewController.self)) as? WeatherDetailsViewController else {
            debugPrint("View Not Found")
            return
        }
        weatherDetailsVC.weatherData = weatherObj
        self.navigationController?.pushViewController(weatherDetailsVC, animated: true)
    }
    
}


extension WeatherTableViewController {
    
    // Remove the activity indicator
    private func removeLoadingScreen() {
        spinner.stopAnimating()
        spinner.isHidden = true
        loadingView.removeFromSuperview()
    }
    
    // Set the activity indicator into the main view
    private func setLoadingScreen() {
        loadingView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        loadingView.center = tableView.center
        spinner.style = .gray
        spinner.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        spinner.startAnimating()
        
        loadingView.addSubview(spinner)
        tableView.addSubview(loadingView)
    }

}


extension WeatherTableViewController: NSFetchedResultsControllerDelegate {
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {

    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        print(indexPath?.row)
    }
}
