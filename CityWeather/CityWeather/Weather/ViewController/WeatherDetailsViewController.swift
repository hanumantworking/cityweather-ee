//
//  WeatherDetailsViewController.swift
//  CityWeather
//
//  Created by PM on 12/05/19.
//  Copyright © 2019 Hanumant Shegade. All rights reserved.
//

import UIKit

class WeatherDetailsViewController: UIViewController {

    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblMinTemp: UILabel!
    @IBOutlet weak var lblMaxTemp: UILabel!
    @IBOutlet weak var lblHumidity: UILabel!
    @IBOutlet weak var lblPressure: UILabel!
    @IBOutlet weak var lblTemprature: UILabel!
    @IBOutlet weak var lblUpdatedAt: UILabel!
    
    
  
    @IBOutlet var weatherDetailsViewModel: WeatherDetailsViewModel!
    var weatherData: Weather!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard weatherData != nil else {
            fatalError("weatherData not found")
        }
        setupUI()
    }
    
    func setupUI() {
        lblCity.text = weatherData.cityName
        lblDescription.text = weatherData.tempratureDescription
        lblMinTemp.text = String(format: "%.1f", weatherData.minTemprature)
        lblMaxTemp.text = String(format: "%.1f", weatherData.maxTemprature)
        lblHumidity.text = "\(weatherData.humidity)%"
        lblPressure.text = "\(weatherData.pressure)hPa"
        lblTemprature.attributedText = getTempratureText(bigString: "\(weatherData.temprature)", smallString: "o")
        
        lblUpdatedAt.text = weatherDetailsViewModel.getUpdatedTime(updatedAt: weatherData.updatedAt) ?? ""
    }
    
}
