//
//  WeatherDetailsViewModel.swift
//  CityWeather
//
//  Created by PM on 13/05/19.
//  Copyright © 2019 Hanumant Shegade. All rights reserved.
//

import UIKit

class WeatherDetailsViewModel: NSObject {
    
    func getUpdatedTime(updatedAt: Date?) -> String? {
        guard let date = updatedAt else {return nil}
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.ReferenceType.local
        dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss"
        let formattedTime = dateFormatter.string(from: date)
        return formattedTime
    }

}
