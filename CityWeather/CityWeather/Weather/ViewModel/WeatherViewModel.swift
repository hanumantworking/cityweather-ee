//
//  WeatherViewModel.swift
//  CityWeather
//
//  Created by PM on 12/05/19.
//  Copyright © 2019 Hanumant Shegade. All rights reserved.
//

import UIKit

class WeatherViewModel: NSObject {
    
    var cityIds: [String] {
        return ["4163971", "2147714", "2174003"]
    }

    var openWeatherURL: URL? {
        let ids = cityIds.joined(separator: ",")
        return URL(string: "\(NetworkConstants.basePath)id=\(ids)&units=metric&APPID=\(NetworkConstants.APIKey)")
    }
    
    func invokeWeatherData(completion: @escaping() -> Void ) {
        
        let queue = DispatchQueue.global(qos: .background)
        queue.async {
            guard let tempratureURL = self.openWeatherURL else {
                fatalError("Wrong weather URL")
            }
            print(tempratureURL)
            NetworkManger.invokeService(url: tempratureURL) { (data, errorMessage) in
                if data != nil {
                    DispatchQueue.main.async {
                        guard let dataArray = data?["list"] as? [AnyObject] else {return}
                        CoreDataManager.saveWeatherData(json: dataArray, completion: {
                            completion()
                        })
                    }
                }
            }
        }
    }
    
    // Fetch all weather data of selected location
    func setWeatherData() {
        let _ = CoreDataManager.fetchEntity(entityClass: Weather.self, setPredicate: nil, fetchLimit: 0) as? [Weather]
    }
}
