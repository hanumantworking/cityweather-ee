//
//  CityTableViewCell.swift
//  CityWeather
//
//  Created by PM on 12/05/19.
//  Copyright © 2019 Hanumant Shegade. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var lblAvgTemprature: UILabel!
    @IBOutlet weak var lblTemprature: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    var weather: Weather? {
        didSet {
            lblCityName.text = weather?.cityName
            if let temp = weather?.temprature {
                lblAvgTemprature.attributedText = getTempratureText(bigString: String(format: "%.1f", temp), smallString: "o")
            } else {
                lblAvgTemprature.text = "NA"
            }
            if let minTemp = weather?.minTemprature, let maxTemp = weather?.maxTemprature {
                lblTemprature.text = String(format: "%.1f", minTemp) + " / " + String(format: "%.1f", maxTemp)
            }
            lblDescription.text = weather?.tempratureDescription ?? "NA"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
