//
//  DBMapper.swift
//  CityWeather
//
//  Created by PM on 12/05/19.
//  Copyright © 2019 Hanumant Shegade. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager {
    
   class func fetchEntity<T: NSManagedObject>(entityClass:T.Type, setPredicate:NSPredicate? = nil, fetchLimit:Int = 0, sortByKey: String? = nil) -> [AnyObject] {
        let entityName = String(describing: entityClass)
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
    
        // Check fetch predicate
        if setPredicate != nil {
            fetchRequest.predicate = setPredicate
        }
        
        fetchRequest.returnsObjectsAsFaults = false
    
        // Check fetch limit
        if fetchLimit > 0 {
            fetchRequest.fetchLimit = fetchLimit
        }
    
        // Sort object by key
        if sortByKey != nil {
            let sectionSortDescriptor = NSSortDescriptor(key: sortByKey, ascending: false)
            let sortDescriptors = [sectionSortDescriptor]
            fetchRequest.sortDescriptors = sortDescriptors
        }
        var result:[AnyObject] = []
        do {
            result = try APP_DELEGATE.MOC.fetch(fetchRequest)
        } catch let error as NSError {
            debugPrint("error in fetchrequest is",error)
            result = []
        }
        return result
    }
    
   class func saveWeatherData(json: [AnyObject], completion: () -> Void) {
        let storedData = fetchEntity(entityClass: Weather.self) as! [Weather]
        for data in json {
            let weatherCityId = data.value(forKey: "id") as? Int32 ?? 0
            let filterObjects = storedData.filter { $0.cityId == weatherCityId }
            
            if filterObjects.isEmpty {
                // Insert Record
                let obj = (NSEntityDescription.insertNewObject(forEntityName: String(describing: Weather.self), into: APP_DELEGATE.MOC) as! Weather)
                obj.save(json: data)
            } else {
                let newTemprature = data.value(forKeyPath: "main.temp") as? Double ?? 0
                if filterObjects.first?.temprature != newTemprature {
                    // Update Record
                    filterObjects.first?.save(json: data)
                }
            }
        }
        do {
            try APP_DELEGATE.MOC.save()
        } catch let error as NSError  {
            print("error in save weather : \(error),  #####info:\(error.userInfo)")
            fatalError("Save DB Error")
        }
        completion()
    }

}






