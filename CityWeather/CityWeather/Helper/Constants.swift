//
//  Constants.swift
//  CityWeather
//
//  Created by PM on 12/05/19.
//  Copyright © 2019 Hanumant Shegade. All rights reserved.
//

import Foundation
import UIKit


let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate


enum NetworkConstants {
    static let APIKey = "2f39d025e01044447c03cc79cc064912"
    static let basePath = "http://api.openweathermap.org/data/2.5/group?"
}
