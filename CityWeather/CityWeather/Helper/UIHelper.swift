//
//  UIHelper.swift
//  CityWeather
//
//  Created by PM on 13/05/19.
//  Copyright © 2019 Hanumant Shegade. All rights reserved.
//

import Foundation
import UIKit


// Get temprature degree symbol in top alignment
func getTempratureText(bigString: String, smallString: String) -> NSMutableAttributedString {
    let fullString = "\(bigString) \(smallString)"
    let string = NSMutableAttributedString(string: fullString)
    
    let bigStringRange = NSRange(location: 0, length: bigString.utf16.count)
    let smallStringRange = NSRange(location: bigStringRange.length + 1, length: smallString.utf16.count)
    
    let bigStringFontSize: CGFloat = 28
    let smallStringFontSize: CGFloat = 18
    
    string.beginEditing()
    
    string.addAttribute(.font, value: UIFont.systemFont(ofSize: bigStringFontSize), range: bigStringRange)
    string.addAttribute(.font, value: UIFont.systemFont(ofSize: smallStringFontSize), range: smallStringRange)
    string.addAttribute(.baselineOffset, value: (bigStringFontSize - smallStringFontSize) / 2, range: smallStringRange)
    
    string.endEditing()
    
    return string
}
