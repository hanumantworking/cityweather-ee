//
//  DBMapper.swift
//  CityWeather
//
//  Created by PM on 12/05/19.
//  Copyright © 2019 Hanumant Shegade. All rights reserved.
//

import Foundation


extension Weather {
    func save(json: AnyObject) {
        if let maxTemp = json.value(forKeyPath: "main.temp_max") as? Double {
            maxTemprature = maxTemp
        }
        if let minTemp = json.value(forKeyPath: "main.temp_min") as? Double {
            minTemprature = minTemp
        }
        if let currentTemp = json.value(forKeyPath: "main.temp") as? Double {
            temprature = currentTemp
        }
        if let airHumidity = json.value(forKeyPath: "main.humidity") as? Int16 {
            humidity = airHumidity
        }
        if let airPressure = json.value(forKeyPath: "main.pressure") as? Int16 {
            pressure = airPressure
        }
    
        let weatherArray = json.value(forKey: "weather") as? [AnyObject]
        tempratureDescription = weatherArray?.first?.value(forKeyPath: "main") as? String
        
        cityName = json.value(forKey: "name") as? String
        cityId = json.value(forKey: "id") as? Int32 ?? 0
        updatedAt = Date()
    }
}
