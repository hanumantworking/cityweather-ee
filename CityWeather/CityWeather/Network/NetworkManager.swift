//
//  NetworkManager.swift
//  CityWeather
//
//  Created by PM on 12/05/19.
//  Copyright © 2019 Hanumant Shegade. All rights reserved.
//

import Foundation
import UIKit

class NetworkManger {
    
    class func invokeService(url: URL, completion:@escaping (AnyObject?, String?) -> Void) {
        
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            print(String(data: data, encoding: .utf8)!)
            
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            
            guard error == nil else {
                // Received error from server
                completion(nil, error?.localizedDescription)
                return
            }
            /*
            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                return
            }
             */

            do {
                let dataArray =  try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
                // Return actual data
                completion(dataArray as AnyObject, nil)
            } catch let error {
                print("Decoding Error: \(error.localizedDescription)")
            }
        }
        
        task.resume()
        
    }

    
}

